document.addEventListener('DOMContentLoaded', function () {
    var checkboxes = document.querySelectorAll('.artwork_cat input[type="checkbox"]');

    function handleCheckboxChange(checkbox) {
        var label = document.querySelector('label[for="' + checkbox.id + '"]');

        if (checkbox.checked) {
            // Ajouter le style lorsque la case est cochée
            label.style.border = '#1b6d85 1px solid';
            label.style.boxShadow = '0 0 10px 5px rgba(33, 245, 255, 0.5)';
        } else {
            // Supprimer le style lorsque la case est décochée
            label.style.border = '#7a7a7a 1px solid';
            label.style.boxShadow = 'none';
        }
    }

    checkboxes.forEach(function (checkbox) {
        // Vérifier l'état initial de la case
        handleCheckboxChange(checkbox);

        checkbox.addEventListener('change', function () {
            // Appeler la fonction de gestion du changement d'état de la case
            handleCheckboxChange(checkbox);
        });
    });
});