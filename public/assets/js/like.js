// Classe Like qui gère le système de likes
export default class Like {
    // Constructeur prenant une liste d'éléments de like en paramètre
    constructor(likeElements) {
        // Stocke les éléments de like
        this.likeElements = likeElements;

        // Vérifie si des éléments de like existent et initialise la classe si c'est le cas
        if (this.likeElements) {
            this.init();
        }
    }

    // Méthode d'initialisation qui ajoute des écouteurs d'événements sur les éléments de like
    init() {
        this.likeElements.forEach(element => {
            element.addEventListener('click', this.onClick.bind(this));
        });
    }

    // Méthode appelée lorsqu'un élément de like est cliqué
    onClick(event) {
        // Empêche le comportement par défaut du lien
        event.preventDefault();

        // Récupère l'URL associée à l'événement de clic
        const url = event.currentTarget.href;

        // Déclaration de la variable span
        let span;

        // Effectue une requête fetch vers l'URL
        fetch(url)
            .then(response => response.json())
            .then(data => {
                // Récupère le nombre de likes depuis la réponse JSON
                const number = data.numberLike;

                // Sélectionne le span qui affiche le nombre de likes
                span = document.querySelector('span.count-like');

                // Met à jour les attributs et le texte du span avec le nouveau nombre de likes
                span.dataset.number = number;
                span.innerText = number + ' J\'aime';

                // Sélectionne les icônes de pouce en l'air rempli et non rempli
                const thumbsUpFilled = document.querySelector('i.bi-heart-fill');
                const thumbsUpUnFilled = document.querySelector('i.bi-heart');

                // Bascule la classe 'hidden' pour afficher ou masquer les icônes
                thumbsUpFilled.classList.toggle('hidden');
                thumbsUpUnFilled.classList.toggle('hidden');
            });
    }
}
