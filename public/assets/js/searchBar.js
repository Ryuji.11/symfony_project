// Sélection des éléments du DOM
let searchBar = document.querySelector('.search-bar');
let searchIcon = document.querySelector('.search-icon');

// Fonction pour basculer l'affichage de la barre de recherche
function toggleSearchBar() {
    // Toggle la classe 'active' pour afficher/cacher la barre de recherche
    searchBar.classList.toggle('active');

    // Toggle la classe 'search-active' sur le header pour ajuster la hauteur
    let header = document.querySelector('.header');
    header.classList.toggle('search-active');

    // Sélection du .hero pour ajuster la hauteur (assurez-vous que 'hero' est défini dans votre code)
    let hero = document.querySelector('.hero');
    hero.classList.toggle('hero-padding');

    // Toggle la classe 'icon-color' pour changer la couleur de l'icône de recherche
    searchIcon.classList.toggle('icon-color');
}

// Sélection de la liste des catégories
const categorieList = document.getElementById('categorie-list');

// Gestionnaire d'événement pour basculer la classe 'selected' sur les éléments de la liste des catégories
categorieList.addEventListener('click', (event) => {
    const selectedCategory = event.target;
    selectedCategory.classList.toggle('selected');
});



