// Sélection des éléments du DOM
let bioForm = document.getElementById('bio-form');
let buttonAdd = document.getElementById('add');
let buttonUpdate = document.getElementById('update');
let buttonDelete = document.getElementById('delete');
let bioText = document.getElementById('bio-text');
let formVisible = false;

// Gestionnaire d'événement pour le bouton "Ajouter"
buttonAdd.addEventListener('click', toggleForm);

// Vérification de l'existence de la balise p avec id "bio-text"
if (bioText !== null) {
    // Vérification si le contenu de la balise p n'est pas vide
    if (bioText.textContent.trim() !== '') {
        // Si la balise p a du texte, désactive le bouton "Ajouter" et retire son gestionnaire d'événement
        buttonAdd.style.color = 'red';
        buttonAdd.style.cursor = 'default';
        buttonAdd.removeEventListener("click", toggleForm);
    }
} else {
    // Si la balise p n'existe pas, ajoute des gestionnaires d'événements aux boutons "Mettre à jour" et "Supprimer"
    buttonUpdate.addEventListener('click', stopEvent);
    buttonDelete.addEventListener('click', stopEvent);

    // Désactive les boutons "Mettre à jour" et "Supprimer"
    buttonUpdate.style.color = 'red';
    buttonUpdate.style.cursor = 'default';
    buttonDelete.style.color = 'red';
    buttonDelete.style.cursor = 'default';

    // Fonction pour arrêter la propagation de l'événement
    function stopEvent(event) {
        event.preventDefault();
    }
}

// Fonction pour basculer l'affichage du formulaire
function toggleForm() {
    formVisible = !formVisible;

    // Affiche ou masque le formulaire en fonction de formVisible
    if (formVisible) {
        bioForm.style.display = 'block';
    } else {
        bioForm.style.display = 'none';
    }

    // Empêche le comportement par défaut du lien
    event.preventDefault();
}
