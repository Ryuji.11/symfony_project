// Fonction pour basculer l'ombre de la boîte
function toggleBoxShadow(element) {
    // Vérifie si l'ombre est actuellement activée ou non, puis inverse l'état
    element.style.boxShadow = (element.style.boxShadow === 'none' || element.style.boxShadow === '') ? '0 0 10px 5px rgba(33, 245, 255, 0.5)' : 'none';
}

// Fonction pour basculer l'affichage d'un élément
function toggleDisplay(element) {
    // Vérifie si l'élément est actuellement masqué ou non, puis inverse l'état
    element.style.display = (element.style.display === 'none' || element.style.display === '') ? 'block' : 'none';
}

// Fonction pour basculer la réponse à une question
function toggleAnswer(questionNumber) {
    // Sélectionne les éléments concernés par la question spécifique
    const line = document.getElementById(`response-box${questionNumber}`);
    const question = document.getElementById(`question${questionNumber}`);

    // Applique les fonctions de bascule sur les éléments sélectionnés
    toggleBoxShadow(question);
    toggleDisplay(line);
}
