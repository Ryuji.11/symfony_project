// Importation de la classe Like depuis le fichier like.js
import Like from './like.js';

// Attente du chargement du contenu de la page
document.addEventListener('DOMContentLoaded', () => {
    // Message de test dans la console pour vérifier le chargement du script

    // Sélection de tous les éléments <a> avec l'attribut data-action="like"
    const likeElements = [].slice.call(document.querySelectorAll('a[data-action="like"]'));

    // Vérification de l'existence d'éléments like sur la page
    if (likeElements) {
        // Instanciation de la classe Like avec les éléments like sélectionnés
        new Like(likeElements);
    }
});