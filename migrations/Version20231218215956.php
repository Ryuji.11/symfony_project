<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231218215956 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE artwork (id INT AUTO_INCREMENT NOT NULL, photo_id INT NOT NULL, member_id INT NOT NULL, artwork_title VARCHAR(255) NOT NULL, creation_date DATETIME NOT NULL, UNIQUE INDEX UNIQ_881FC5767E9E4C8C (photo_id), INDEX IDX_881FC5767597D3FE (member_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE artwork_member (artwork_id INT NOT NULL, member_id INT NOT NULL, INDEX IDX_8D17E96ADB8FFA4 (artwork_id), INDEX IDX_8D17E96A7597D3FE (member_id), PRIMARY KEY(artwork_id, member_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE biography (id INT AUTO_INCREMENT NOT NULL, member_id INT DEFAULT NULL, biography LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_E3B3665C7597D3FE (member_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, category_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_artwork (category_id INT NOT NULL, artwork_id INT NOT NULL, INDEX IDX_4FC7850812469DE2 (category_id), INDEX IDX_4FC78508DB8FFA4 (artwork_id), PRIMARY KEY(category_id, artwork_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, member_id INT NOT NULL, artwork_id INT NOT NULL, commentary LONGTEXT NOT NULL, date DATETIME NOT NULL, INDEX IDX_9474526C7597D3FE (member_id), INDEX IDX_9474526CDB8FFA4 (artwork_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, member_id INT DEFAULT NULL, message LONGTEXT NOT NULL, subject VARCHAR(255) NOT NULL, date DATETIME NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_4C62E6387597D3FE (member_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE faq (id INT AUTO_INCREMENT NOT NULL, question LONGTEXT NOT NULL, response LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE member (id INT AUTO_INCREMENT NOT NULL, nametag VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', name VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, picture VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, member_id INT NOT NULL, artwork_id INT NOT NULL, report_message LONGTEXT NOT NULL, report_subject VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_C42F77847597D3FE (member_id), INDEX IDX_C42F7784DB8FFA4 (artwork_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_network (id INT AUTO_INCREMENT NOT NULL, member_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL, social_network_link VARCHAR(255) DEFAULT NULL, INDEX IDX_EFFF52217597D3FE (member_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT FK_881FC5767E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id)');
        $this->addSql('ALTER TABLE artwork ADD CONSTRAINT FK_881FC5767597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE artwork_member ADD CONSTRAINT FK_8D17E96ADB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artwork_member ADD CONSTRAINT FK_8D17E96A7597D3FE FOREIGN KEY (member_id) REFERENCES member (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE biography ADD CONSTRAINT FK_E3B3665C7597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE category_artwork ADD CONSTRAINT FK_4FC7850812469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_artwork ADD CONSTRAINT FK_4FC78508DB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CDB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E6387597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77847597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784DB8FFA4 FOREIGN KEY (artwork_id) REFERENCES artwork (id)');
        $this->addSql('ALTER TABLE social_network ADD CONSTRAINT FK_EFFF52217597D3FE FOREIGN KEY (member_id) REFERENCES member (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE artwork DROP FOREIGN KEY FK_881FC5767E9E4C8C');
        $this->addSql('ALTER TABLE artwork DROP FOREIGN KEY FK_881FC5767597D3FE');
        $this->addSql('ALTER TABLE artwork_member DROP FOREIGN KEY FK_8D17E96ADB8FFA4');
        $this->addSql('ALTER TABLE artwork_member DROP FOREIGN KEY FK_8D17E96A7597D3FE');
        $this->addSql('ALTER TABLE biography DROP FOREIGN KEY FK_E3B3665C7597D3FE');
        $this->addSql('ALTER TABLE category_artwork DROP FOREIGN KEY FK_4FC7850812469DE2');
        $this->addSql('ALTER TABLE category_artwork DROP FOREIGN KEY FK_4FC78508DB8FFA4');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7597D3FE');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CDB8FFA4');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E6387597D3FE');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F77847597D3FE');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784DB8FFA4');
        $this->addSql('ALTER TABLE social_network DROP FOREIGN KEY FK_EFFF52217597D3FE');
        $this->addSql('DROP TABLE artwork');
        $this->addSql('DROP TABLE artwork_member');
        $this->addSql('DROP TABLE biography');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_artwork');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE faq');
        $this->addSql('DROP TABLE member');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE social_network');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
