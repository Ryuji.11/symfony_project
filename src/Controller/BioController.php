<?php

namespace App\Controller;

use App\Form\BiographyFormType;
use App\Repository\BiographyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BioController extends AbstractController
{
    #[Route('/doctrine/delete/biography/{id}', name: 'delete_biography', methods: ['GET', 'POST'])]
    public function DeleteBio(int $id, EntityManagerInterface $entityManager, BiographyRepository $biographyRepository): Response
    {
        // Méthode pour supprimer une biographie
        $bio = $biographyRepository->find($id);
        $entityManager->remove($bio);
        $entityManager->flush();

        return $this->redirectToRoute('biography');
    }

    #[Route('/doctrine/update/biography/{id}', name: 'update_biography', methods: ['GET', 'POST'])]
    public function updateBio(int $id, EntityManagerInterface $entityManager, BiographyRepository $biographyRepository, Request $request): Response
    {
        // Méthode pour mettre à jour une biographie
        $bio = $biographyRepository->find($id);
        $form = $this->createForm(BiographyFormType::class, $bio);
        //Gestion du formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Envoie les nouvelle données du formulaire dans le db
            $entityManager->flush();
            //Redirige l'utilisateur vers la page de sa biographie
            return $this->redirectToRoute('biography');
        }

        // Affiche le rendu visuelle de la page
        return $this->render('update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'Modification de la bio'
        ]);
    }
}
