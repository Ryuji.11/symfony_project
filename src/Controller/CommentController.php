<?php

namespace App\Controller;

use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    #[Route('/doctrine/delete/comment/{id}', name: 'delete_comment', methods: ['GET', 'POST'])]
    public function DeleteMember(int $id, EntityManagerInterface $entityManager, CommentRepository $commentRepository): Response
    {
        //logique de suppression de la category
        $comment = $commentRepository->find($id);
        $entityManager->remove($comment);
        $entityManager->flush();

        // redirige vers la page d'administration des message après suppression
        return $this->redirectToRoute('comment');
    }
}
