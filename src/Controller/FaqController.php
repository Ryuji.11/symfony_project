<?php

namespace App\Controller;

use App\Form\FaqFormType;
use App\Repository\FaqRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FaqController extends AbstractController
{
    #[Route('/doctrine/update/faq/{id}', name: 'update_faq')]
    public function updateFaq(int $id, FaqRepository $faqRepository, Request $request, EntityManagerInterface $entityManager): Response
    {
        // Récupère l'objet Faq à mettre à jour en fonction de son identifiant
        $faq = $faqRepository->find($id);

        // Crée un formulaire de type FaqFormType associé à l'objet Faq
        $form = $this->createForm(FaqFormType::class, $faq);

        // Gère la soumission du formulaire
        $form->handleRequest($request);

        // Vérifie si le formulaire a été soumis et s'il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Enregistre les changements dans la base de données
            $entityManager->flush();

            // Redirige vers la page d'ajout de FAQ
            return $this->redirectToRoute('add_faq');
        }

        // Affiche la page de formulaire de mise à jour avec le formulaire et le nom de la page
        return $this->render('update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'Modification du FAQ'
        ]);
    }

    #[Route('/doctrine/delete/faq/{id}', name: 'delete_faq')]
    public function deleteFaq(int $id, EntityManagerInterface $entityManager, FaqRepository $faqRepository): Response
    {
        // Récupère l'objet Faq à supprimer en fonction de son identifiant
        $faq = $faqRepository->find($id);

        // Supprime l'objet Faq de la base de données
        $entityManager->remove($faq);
        $entityManager->flush();

        // Redirige vers la page d'ajout de FAQ
        return $this->redirectToRoute('add_faq');
    }
}