<?php

namespace App\Controller;

use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/doctrine/delete/contact/{id}', name: 'delete_contact', methods: ['GET', 'POST'])]
    public function DeleteMember(int $id, EntityManagerInterface $entityManager, ContactRepository $contactRepository): Response
    {
        //logique de suppression de la category
        $contact = $contactRepository->find($id);
        $entityManager->remove($contact);
        $entityManager->flush();

        // redirige vers la page d'administration des message après suppression
        return $this->redirectToRoute('contact_message');
    }
}
