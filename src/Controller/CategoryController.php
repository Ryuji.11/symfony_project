<?php

namespace App\Controller;

use App\Form\CreateCategoryFormType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    #[Route('/doctrine/update/category/{id}', name: 'update_category', methods: ['GET', 'POST'])]
    public function updateMember(int $id, EntityManagerInterface $entityManager, CategoryRepository $categoryRepository, Request $request ): Response
    {
        // logique de mise à jour de la category
        $category = $categoryRepository->find($id);
        $form = $this->createForm(CreateCategoryFormType::class, $category);
        // Gestion du formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            // Enregistre les modifications si le formulaire est soumis et valide
            $entityManager->flush();
            // Redirige vers la page d'administration des catégorie
            return $this->redirectToRoute('category');
        }

        // Récupère toutes les catégories pour l'affichage
        $entities = $categoryRepository->findAll();
        return $this->render('update_form_page/update-form.html.twig', [
            'entities'=>$entities,
            'updateForm'=>$form->createView(),
            'pageUpdateName' => 'Modification des catégories',
        ]);

    }

    #[Route('/doctrine/delete/category/{id}', name: 'delete_category', methods: ['GET', 'POST'])]
    public function DeleteCategory(int $id, EntityManagerInterface $entityManager, CategoryRepository $categoryRepository): Response
    {
        // Logique de suppression de la category
        $category = $categoryRepository->find($id);
        $entityManager->remove($category);
        $entityManager->flush();

        // redirige vers la page d'administration des catégorie après la suppression
        return $this->redirectToRoute('category');
    }
}
