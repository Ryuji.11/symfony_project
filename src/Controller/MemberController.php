<?php

namespace App\Controller;

use App\Form\UpdateMailFormType;
use App\Form\UpdateMemberAdminFormType;
use App\Form\UpdatePasswordFormType;
use App\Repository\MemberRepository;
use App\Security\AppCustomAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class MemberController extends AbstractController
{
    #[Route('/doctrine/update-admin/member/{id}', name: 'update_admin', methods: ['GET', 'POST'])]
    public function updateAdminMember(int $id, EntityManagerInterface $entityManager, Request $request, MemberRepository $memberRepository): Response
    {
        // Récupérer le membre à mettre à jour
        $member = $memberRepository->find($id);
        // Créer le formulaire de mise à jour
        $form = $this->createForm(UpdateMemberAdminFormType::class, $member);
        // Gérer la soumission du formulaire
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            // Enregistrer les modifications dans la base de données
            $entityManager->flush();
            // Rediriger vers la page principale des membres
            return $this->redirectToRoute('member');
        }
        // Afficher la vue avec le formulaire de mise à jour
        return $this->render('update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'changement du statut admin'
        ]);
    }

    #[Route('/doctrine/delete/member/{id}', name: 'delete_member', methods: ['GET', 'POST'])]
    public function DeleteMember(int $id, EntityManagerInterface $entityManager, MemberRepository $memberRepository, Security $security): Response
    {
        // Récupérer le membre à supprimer
        $member = $memberRepository->find($id);

        // vérifie si le membre à un role admin et si oui affiche un message d'erreur empêchant la suppression de l'utilisateur
        if (in_array('ROLE_ADMIN', $member->getRoles())) {
            $this->addFlash('error', 'cette utilisateur ne peut pas être supprimer');
        }else{
            // Supprimer le membre de la base de données
            $entityManager->remove($member);
            $entityManager->flush();
        }

        // Rediriger selon le rôle de l'utilisateur
        if ($security->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('member');
        } else {
            return $this->redirectToRoute('gallery');
        }

    }

    #[Route('/doctrine/update-password/member/{id}', name: 'update_member_password', methods: ['GET', 'POST'])]
    public function updatePasswordMember(int $id, EntityManagerInterface $entityManager, Request $request, UserPasswordHasherInterface $passwordHasher, MemberRepository $memberRepository, AppCustomAuthenticator $authenticator, UserAuthenticatorInterface $userAuthenticator): Response
    {
        // Récupérer le membre à mettre à jour
        $member = $memberRepository->find($id);
        // Créer le formulaire de mise à jour
        $form = $this->createForm(UpdatePasswordFormType::class, $member);
        // Gérer la soumission du formulaire
        $form->handleRequest($request);

        // Vérifie si le formulaire a été posté et si il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupère l'ancien mot de passe via le formulaire
            $oldPassword = $form->get('oldPassword')->getData();
            // Vérifie si le mot de passe saisi concorde avec le mot de passe de stocker dans la db
            if ($passwordHasher->isPasswordValid($member, $oldPassword)) {
                // Récupère le mot nouveau mot de passe et la confirmation
                $newPassword = $form->get('newPassword')->getData();
                $confirmPassword = $form->get('confirmPassword')->getData();
                //  Vérifie si le nouveau mot de passe correspond avec la confirmation mot de passe
                if ($newPassword === $confirmPassword) {

                    //Si la confirmation du nouveau mot de passe est correcte alors le mot de passe est crypter et remplace l'ancien
                    $member->setPassword(
                        $passwordHasher->hashPassword(
                            $member,
                            $newPassword
                        )
                    );
                    // Sauvegarde en base de données
                    $entityManager->flush();

                    // Ajoute une message de confirmation
                    $this->addFlash('success', 'le mot de passe a été changer avec succès');

                    $userAuthenticator->authenticateUser(
                        $member,
                        $authenticator,
                        $request
                    );

                    // Redirigez l'utilisateur vers la page de profil
                    return $this->redirectToRoute('account');
                } else {
                    // Ajoute une message d'erreur si la correspondance du nouveau mot de passe n'est pas correcte
                    $this->addFlash('error', 'Les nouveaux mots de passe ne correspondent pas.');
                }
            } else {
                // Ajoute un message d'erreur si l'ancien mot de passe n'est pas correcte
                $this->addFlash('error', 'Le mot de passe actuel est incorrect.');
            }

        }
        // Afficher la vue avec le formulaire de mise à jour
        return $this->render('update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'changement du mot de passe'
        ]);
    }

    #[Route('/doctrine/update-mail/member/{id}', name: 'update_member_mail', methods: ['GET', 'POST'])]
    public function updateMailMember(int $id, EntityManagerInterface $entityManager, Request $request, MemberRepository $memberRepository): Response
    {
        // Récupérer le membre à mettre à jour
        $member = $memberRepository->find($id);
        // Créer le formulaire de mise à jour
        $form = $this->createForm(UpdateMailFormType::class, $member);
        // Gérer la soumission du formulaire
        $form->handleRequest($request);

        // Vérifie si le formulaire a été posté et si il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // si le form est valide, sauvegarde le nouveau mail en db
            $entityManager->flush();
            // Ajoute une message de confirmation
            $this->addFlash('success', 'l\'Email a été changer avec succès');
            // Redirigez l'utilisateur vers la page de profil
            return $this->redirectToRoute('account');
        }
        // Afficher la vue avec le formulaire de mise à jour
        return $this->render('update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'changement de l\'adresse email'
        ]);
    }
}