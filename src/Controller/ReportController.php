<?php

namespace App\Controller;

use App\Repository\ReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    #[Route('/doctrine/delete/report/{id}', name: 'delete_report', methods: ['GET', 'POST'])]
    public function DeleteMember(int $id, EntityManagerInterface $entityManager, ReportRepository $reportRepository ): Response
    {
        //logique de suppression du report
        $report = $reportRepository->find($id);
        $entityManager->remove($report);
        $entityManager->flush();

        // redirige vers la page d'administration des reports après suppression
        return $this->redirectToRoute('report_message');
    }
}
