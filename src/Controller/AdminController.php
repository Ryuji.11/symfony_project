<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Faq;
use App\Form\CreateCategoryFormType;
use App\Form\FaqFormType;
use App\Repository\ArtworkRepository;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use App\Repository\ContactRepository;
use App\Repository\FaqRepository;
use App\Repository\MemberRepository;
use App\Repository\PhotoRepository;
use App\Repository\ReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    #[Route('/doctrine/admin-interface', name: 'admin_interface')]
    public function adminInterface(): Response
    {
        // Méthode pour afficher l'interface d'administration
        return $this->render('admin-interface/admin-interface.html.twig');
    }

    #[Route('/doctrine/admin-interface/member', name: 'member')]
    public function member( MemberRepository $memberRepository): Response
    {
        // Méthode pour afficher et gérer les membres dans l'interface d'administration
        $entities = $memberRepository->findall();
        return $this->render('admin-interface/member.html.twig', [
            'entities' => $entities,
        ]);
    }

    #[Route('/doctrine/admin-interface/category', name: 'category')]
    public function createCategory(EntityManagerInterface $entityManager, CategoryRepository $categoryRepository, Request $request): Response
    {
        //Méthode pour créer les catégories
        $category = new Category();
        //Création du formulaire pour les catégories
        $form = $this->createForm(CreateCategoryFormType::class, $category);
        //Gérer la soumission du formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            //Si le formulaire est valide, enregistre les donner de ce dernier et les envoies dans la db
            $entityManager->persist($category);
            $entityManager->flush();
            //Redirige vers la page d'administration des catégories
            return $this->redirectToRoute('category');
        }
        // Afficher les catégories dans l'interface d'administration
        $entities = $categoryRepository->findAll();
        return $this->render('admin-interface/category.html.twig', [
            'entities'=>$entities,
            'categoryForm'=>$form->createView(),
        ]);
    }

    #[Route('/doctrine/admin-interface/artwork', name: 'show_artwork')]
    public function addArtwork(ArtworkRepository $artworkRepository): Response
    {
        // Méthode pour afficher les œuvres dans l'interface d'administration
        $entities = $artworkRepository->findAll();
        return $this->render('admin-interface/artwork.html.twig', [
            'entities' => $entities,
        ]);
    }

    #[Route('/doctrine/admin-interface/comment', name: 'comment')]
    public function comment(CommentRepository $commentRepository): Response
    {
        // Méthode pour afficher les commentaires dans l'interface d'administration
        $entities = $commentRepository->findAll();
        return $this->render('admin-interface/comment.html.twig', [
        'entities' => $entities,
        ]);
    }

    #[Route('/doctrine/admin-interface/report', name: 'report_message')]
    public function report(ReportRepository $reportRepository): Response
    {
        // Méthode pour afficher les rapports dans l'interface d'administration
        $entities = $reportRepository->findAll();
        return $this->render('admin-interface/report.html.twig', [
            'entities' => $entities
        ]);
    }

    #[Route('/doctrine/admin-interface/faq', name: 'add_faq')]
    public function addFaq(FaqRepository $faqRepository, FaqFormType $faqFormType, EntityManagerInterface $entityManager, Request $request): Response
    {
        // Méthode pour ajouter les FAQ
        $faq = new Faq();
        // Création du formulaire du FAQ
        $form = $this->createForm($faqFormType::class, $faq );
        //Gérer la soumission du formulaire
        $form->handleRequest($request);
        if($form->IsSubmitted() && $form->isValid()){
            //Si le formulaire est valide alors enregistre les données saisie et envoie les dans la db
            $entityManager->persist($faq);
            $entityManager->flush();
            //Redirige vers la page de gestion des FAQ
            return $this->redirectToRoute('add_faq');
        }

        // Affiche les FAQ dans l'interface d'administration
        $entities = $faqRepository->findAll();
        return $this->render('admin-interface/add-faq.html.twig', [
            'entities' => $entities,
            'faqForm' => $form->createView(),
        ]);
    }

    #[Route('/doctrine/admin-interface/contact', name: 'contact_message')]
    public function contactMessage(ContactRepository $contactRepository): Response
    {
        // Méthode pour afficher les messages de contact dans l'interface d'administration
        $entities = $contactRepository->findAll();
        return $this->render('admin-interface/contact.html.twig', [
            'entities' => $entities,
        ]);
    }

    #[Route('/doctrine/admin-interface/photo', name: 'photo')]
    public function addPhoto(PhotoRepository $photoRepository): Response
    {
        // Affiche la page d'administration des images artwork
        $entities = $photoRepository->findAll();
        return $this->render('admin-interface/photo.html.twig', [
            'entities' => $entities,
        ]);
    }
}
