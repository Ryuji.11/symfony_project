<?php

namespace App\Controller;

use App\Form\UpdateArtworkFormType;
use App\Repository\ArtworkRepository;
use App\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArtworkController extends AbstractController
{
    #[Route('/doctrine/update/artwork/{id}', name: 'update_artwork', methods: ['GET', 'POST'])]
    public function updateArtwork(int $id, ArtworkRepository $artworkRepository, Request $request, EntityManagerInterface $entityManager): Response
    {
        // Récupère l'œuvre d'art à mettre à jour
        $artwork = $artworkRepository->find($id);
        // Créer le formulaire de mise à jour
        $form = $this->createForm(UpdateArtworkFormType::class, $artwork);
        // Gérer la soumission du formulaire
        $form->handleRequest($request);

        // Enregistre les modifications dans la base de données et redirige vers la page des œuvres d'art de l'artiste
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer les catégories sélectionnées dans le formulaire
            $selectedCategories = $form->get('categories')->getData();

            // Ajoute l'artwork aux catégories sélectionnées
            foreach ($selectedCategories as $category) {
                $category->addArtwork($artwork);
                $entityManager->persist($category);
            }

            // Envoie les donnée dans la db
            $entityManager->flush();
            return $this->redirectToRoute('artwork');
        }

        // Renvoie vers la vue de la page update
        return $this->render('/update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'Modification de l\'artwork'
        ]);
    }

    #[Route('/doctrine/remove-cat/artwork/{id}', name: 'remove_cat_artwork', methods: ['GET', 'POST'])]
    public function removeCatArtwork(int $id, ArtworkRepository $artworkRepository, Request $request, EntityManagerInterface $entityManager): Response
    {
        // Récupère l'œuvre d'art à mettre à jour
        $artwork = $artworkRepository->find($id);
        // Créer le formulaire de mise à jour
        $form = $this->createForm(UpdateArtworkFormType::class, $artwork);
        // Gérer la soumission du formulaire
        $form->handleRequest($request);

        // Enregistre les modifications dans la base de données et redirige vers la page des œuvres d'art de l'artiste
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer les catégories sélectionnées dans le formulaire
            $selectedCategories = $form->get('categories')->getData();

            // supprime l'artwork aux catégories sélectionnées
            foreach ($selectedCategories as $category) {
                $artwork->removeCategory($category);
                $entityManager->persist($artwork);
            }

            // Envoie les donnée dans la db
            $entityManager->flush();
            return $this->redirectToRoute('artwork');
        }

        // Renvoie vers la vue de la page update
        return $this->render('/update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'Modification de l\'artwork'
        ]);
    }



    #[Route('/doctrine/delete/artwork/{id}', name: 'delete_artwork', methods: ['GET', 'POST'])]
    public function DeleteCategory(int $id, EntityManagerInterface $entityManager, ArtworkRepository $artworkRepository, PhotoRepository $photoRepository, Security $security): Response
    {
        // Récupérer l'œuvre d'art à supprimer
        $artwork = $artworkRepository->find($id);
        // récupère le chemin du fichier de la photo
        $filePath = $this->getParameter('artwork_photo_path') . '/' . $artwork->getPhoto()->getPath();
        // Récupérer l'id de la photo associée à l'œuvre d'art
        $idPhoto = $artwork->getPhoto()->getId();

        // Supprimer le fichier de la photo s'il existe
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        // Supprimer la photo de la base de données
        $photo = $photoRepository->find($idPhoto);
        $entityManager->remove($photo);
        $entityManager->flush();

        // Supprimer l'œuvre d'art de la base de données
        $entityManager->remove($artwork);
        $entityManager->flush();

        // Redirige vers la page des œuvres d'art de l'artiste

        if ($security->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('show_artwork');
        } else {
            return $this->redirectToRoute('artwork');
        }
    }
}
