<?php

namespace App\Controller;

use App\Form\NetworkFormType;
use App\Repository\SocialNetworkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NetworkController extends AbstractController
{
    #[Route('/doctrine/update/network/{id}', name: 'update_network')]
    public function updateNetwork(int $id,  SocialNetworkRepository $networkRepository, Request $request, EntityManagerInterface $entityManager): Response
    {
        // Récupère le réseau social à mettre à jour
        $network = $networkRepository->find($id);

        // Crée un formulaire pour la mise à jour du réseau social
        $form = $this->createForm(NetworkFormType::class, $network);

        // Traite la soumission du formulaire
        $form->handleRequest($request);

        // Vérifie si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()){
            // Enregistre les changements dans la base de données
            $entityManager->flush();

            // Redirige vers la liste des réseaux sociaux
            return $this->redirectToRoute('network');
        }

        // Affiche le formulaire de mise à jour dans le template
        return $this->render('update_form_page/update-form.html.twig', [
            'updateForm' => $form->createView(),
            'pageUpdateName' => 'Modification du réseaux social',
        ]);
    }

    #[Route('/doctrine/delete/network/{id}', name: 'delete_network')]
    public function deleteNetwork(int $id,  SocialNetworkRepository $networkRepository, EntityManagerInterface $entityManager): Response
    {
        // Récupère le réseau social à supprimer
        $network = $networkRepository->find($id);

        // Supprime le réseau social de la base de données
        $entityManager->remove($network);
        $entityManager->flush();

        // Redirige vers la liste des réseaux sociaux
        return $this->redirectToRoute('network');

    }
}
