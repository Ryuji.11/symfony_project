<?php

namespace App\Controller;

use App\Entity\Artwork;
use App\Repository\MemberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LikeController extends AbstractController
{
    #[Route('/like/artwork/{id}', name: 'like_artwork')]
    public function like(Artwork $artwork, Security $security, EntityManagerInterface $entityManager, MemberRepository $memberRepository): Response
    {
        // Récupère l'utilisateur actuellement connecté (membre)
        $member = $security->getUser();
        $memberId = $memberRepository->find($member);

        // Vérifie si l'artwork est déjà liké par l'utilisateur
        if ($artwork->isLikeByUser($memberId)){
            // Si oui, supprime le like de l'utilisateur
            $artwork->removeLike($memberId);
            $entityManager->flush();

            // Retourne une réponse JSON indiquant que le like a été supprimé
            return $this->json([
                'message'=> 'le like a été supprimé',
                'numberLike' => count($artwork->getLikes())
            ]);
        }

        // Si l'artwork n'est pas liké par l'utilisateur, ajoute le like lorsqu'il cliquera sur le bouton
        $artwork->addLike($memberId);
        $entityManager->flush();

        // Retourne une réponse JSON indiquant que le like a été ajouté
        return $this->json([
            'message'=> 'le like a été ajouté',
            'numberLike' => count($artwork->getLikes())
        ]);
    }
}