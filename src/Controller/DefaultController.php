<?php

namespace App\Controller;

use App\Entity\Artwork;
use App\Entity\Biography;
use App\Entity\Comment;
use App\Entity\Contact;
use App\Entity\Photo;
use App\Entity\Report;
use App\Entity\SocialNetwork;
use App\Form\BiographyFormType;
use App\Form\CommentFormType;
use App\Form\ContactFormType;
use App\Form\CreateArtworkFormType;
use App\Form\NetworkFormType;
use App\Form\ReportFormType;
use App\Form\SearchType;
use App\Repository\ArtworkRepository;
use App\Repository\BiographyRepository;
use App\Repository\FaqRepository;
use App\Repository\MemberRepository;
use App\Repository\SocialNetworkRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{
    #[Route('/', name: 'gallery')]
    public function index(ArtworkRepository $artworkRepository, Request $request): Response
    {
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $searchData = $form->getData();

            $artworks = $artworkRepository->searchArtworks($searchData);

            if (empty($artworks)) {
                $this->addFlash('info', 'Aucun résultat trouvé pour la recherche.');
            }

            return $this->render('pages/home.html.twig', [
                'artworks' => $artworks,
                'searchForm' => $form->createView(),
            ]);
        }

        // Récupère tous les artworks pour l'affichage dans la galerie
        $entitiesArtwork = $artworkRepository->findAll();
        return $this->render('pages/home.html.twig', [
            'artworks' => $entitiesArtwork,
            'searchForm' => $form->createView(),
        ]);
    }

    #[Route('/faq', name: 'faq')]
    public function faq(FaqRepository $faqRepository): Response
    {
        // Récupère toutes les FAQ pour les afficher
        $entities = $faqRepository->findAll();
        return $this->render('pages/faq.html.twig', [
            'entities' => $entities,
        ]);
    }

    #[Route('/artwork/{id}', name: 'artwork_page')]
    public function artworkPage(int $id, EntityManagerInterface $entityManager, ArtworkRepository $artworkRepository, Request $request, MemberRepository $memberRepository): Response
    {
        // Méthode qui ajoute les commentaire si il y en a
        $comment = new Comment();
        // Création du form pour les commentaire
        $form = $this->createForm(CommentFormType::class, $comment);
        // Méthode qui crée la nouvelle date a l'heure actuelle ou le commentaire est posté
        $currentDate = new DateTime();
        // Récupère l'utilisateur connecté
        $member = $this->getUser();
        // Récupère l'artwork a afficher via l'id récupérer dans l'URL
        $entity = $artworkRepository->find($id);
        // Récupère les commentaire lié a l'artwork affiché
        $comments = $entity->getComments();

        // Initialise un message d'erreur
        $error = '';
        // Gère la soumission du form
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Si le formulaire est envoyé et et valide alors on vérifie si la personnes est un utilisateur connecté
            if ($member) {
                // Récupère l'id du membre connecté
                $memberId = $memberRepository->find($member);
                // Si le commentaire est soumis par un utilisateur connecter alors on attribue les données collecter au élément constituant le form
                $comment->setMember($memberId);
                $comment->setDate($currentDate);
                $comment->setArtwork($entity);
                // On sauvegarde te envoie les données en DB
                $entityManager->persist($comment);
                $entityManager->flush();
                // Redirige vers la page de l'artwork consulté
                return $this->redirectToRoute('artwork_page', ['id' => $id]);
            } else {
                // Si l'utilisateur n'est pas connecter alors on affiche un message d'erreur
                $error = 'Vous devez être connecté pour pouvoir commenter!';
            }
        }

        // Affiche la page d'un artwork avec la possibilité de laisser des commentaires
        return $this->render('pages/artwork-page.html.twig', [
            'entity' => $entity,
            'comments' => $comments,
            'commentForm' => $form->createView(),
            'error' => $error,
        ]);
    }

    #[Route('/contact', name: 'contact')]
    public function contact(EntityManagerInterface $entityManager, Request $request, MemberRepository $memberRepository): Response
    {
        // Crée une nouvelle instance de l'entité Contact
        $contact = new Contact();
        // Récupère l'utilisateur connecté (membre)
        $member = $this->getUser();
        // Crée un formulaire basé sur le ContactFormType, lié à l'entité Contact
        $form = $this->createForm(ContactFormType::class, $contact);
        // Initialise la date actuelle
        $currentDate = new DateTime();

        // Gère la soumission du formulaire
        $form->handleRequest($request);

        // Vérifie si le formulaire a été soumis et est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Si un membre est connecté, associe le contact au membre
            if ($member) {
                // Récupère l'identifiant du membre à partir du référentiel des membres
                $memberId = $memberRepository->find($member);
                $contact->setMember($memberId);
            }
            // Définit la date du contact sur la date actuelle
            $contact->setDate($currentDate);
            // Persiste le contact dans la base de données
            $entityManager->persist($contact);
            $entityManager->flush();

            // Ajoute un message flash pour informer l'utilisateur du succès de l'envoi
            $this->addFlash('success', 'Votre message a été envoyé. Merci !');

            // Redirige vers la page de contact
            return $this->redirectToRoute('contact');
        }

        // Si le formulaire n'est pas soumis ou n'est pas valide, affiche la page de contact avec le formulaire
        return $this->render('pages/contact.html.twig', [
            'contactForm' => $form,
        ]);
    }

    #[Route('/account', name: 'account')]
    public function account(): Response
    {
        // Affiche la page compte
        return $this->render('pages/account.html.twig');
    }

    #[Route('/my_artwork', name: 'artwork')]
    public function artwork(ArtworkRepository $artworkRepository): Response
    {
        // Récupère l'utilisateur connecté (membre)
        $member = $this->getUser();

        // Vérifie si un membre est connecté
        if ($member) {
            // Récupère les œuvres d'art de l'artiste à partir du référentiel des œuvres d'art
            $artistArtworks = $artworkRepository->findBy(['member' => $member]);

            // Affiche la page 'my_artwork.html.twig' avec les œuvres d'art de l'artiste
            return $this->render('pages/my_artwork.html.twig', [
                'artistArtworks' => $artistArtworks,
            ]);
        }

        // Affiche la page des artworks posté par l'utilisateur connecter
        return $this->render('pages/my_artwork.html.twig', [
            'artistArtworks' => [],
        ]);
    }

    #[Route('/biography', name: 'biography')]
    public function biography(EntityManagerInterface $entityManager, Request $request, BiographyRepository $biographyRepository, MemberRepository $memberRepository): Response
    {
        // Récupère l'utilisateur connecté (membre)
        $member = $this->getUser();
        // Récupère l'ID du membre connecté
        $memberId = $memberRepository->find($member);
        // Crée une nouvelle instance de l'entité Biography
        $biography = new Biography();
        // Crée un formulaire pour la biographie en utilisant BiographyFormType
        $form = $this->createForm(BiographyFormType::class, $biography);
        // Initialise un tableau pour stocker les biographies de l'artiste
        $biographyArtist = [];

        // Gère la soumission du formulaire
        $form->handleRequest($request);

        // Vérifie si un membre est connecté
        if ($member) {
            // Récupère les biographies associées à ce membre
            $biographyArtist = $biographyRepository->findBy(['member' => $member]);
        }

        // Si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Associe la biographie au membre connecté
            $biography->setMember($memberId);

            // Persiste la biographie en base de données
            $entityManager->persist($biography);
            $entityManager->flush();

            // Redirige vers la page 'Biography' après la soumission du formulaire
            return $this->redirectToRoute('biography');
        }

        // Affiche la page 'biography.html.twig' avec le formulaire et la biography de l'artiste
        return $this->render('pages/biography.html.twig', [
            'bioForm' => $form->createView(),
            'biographies' => $biographyArtist,
        ]);
    }

    #[Route('/network', name: 'network')]
    public function network(EntityManagerInterface $entityManager, Request $request, SocialNetworkRepository $networkRepository): Response
    {
        // Récupère l'utilisateur connecté (membre)
        $member = $this->getUser();
        // Récupère l'utilisateur connecté (membre)
        $socialNetwork = new SocialNetwork();
        // Crée un formulaire pour le réseau social en utilisant NetworkFormType
        $form = $this->createForm(NetworkFormType::class, $socialNetwork);
        // Initialise un tableau pour stocker les réseaux sociaux de l'artiste
        $network = [];

        // Gère la soumission du formulaire
        $form->handleRequest($request);

        // Si un membre est connecté
        if ($member) {
            // Récupère les réseaux sociaux associés à ce membre
            $network = $networkRepository->findBy(['member' => $member]);
        }
        // Si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Associe le réseau social au membre connecté
            $socialNetwork->setMember($member);
            // Persiste le réseau social en base de données
            $entityManager->persist($socialNetwork);
            $entityManager->flush();

            // Redirige vers la page 'Network' après la soumission du formulaire
            return $this->redirectToRoute('network');
        }

        // Affiche la page 'network.html.twig' avec le formulaire et les réseaux sociaux de l'artiste
        return $this->render('pages/network.html.twig', [
            'networkForm' => $form->createView(),
            'networkEntities' => $network,
        ]);
    }

    #[Route('/add-artwork', name: 'add_artwork')]
    public function addElement(EntityManagerInterface $entityManager, Request $request): Response
    {
        // Crée une nouvelle instance d'Artwork
        $artwork = new Artwork();
        // Crée une nouvelle instance de Photo
        $image = new Photo();
        // Obtient la date actuelle
        $currentDate = new DateTime();
        // Crée un formulaire pour l'ajout d'œuvre d'art en utilisant CreateArtworkFormType
        $form = $this->createForm(CreateArtworkFormType::class, $artwork);
        // Obtient l'utilisateur connecté (membre)
        $user = $this->getUser();

        // Gère la soumission du formulaire
        $form->handleRequest($request);

        // Si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupérer les catégories sélectionnées dans le formulaire
            $selectedCategories = $form->get('categories')->getData();

            // Ajouter l'artwork aux catégories sélectionnées
            foreach ($selectedCategories as $category) {
                $category->addArtwork($artwork);
                $entityManager->persist($category);
            }

            // uploads de l'image
            /** @var UploadedFile $imageUploadedFile */
            $imageUploadedFile = $form->get('picture')->get('picture')->getData();
            $originalName = $imageUploadedFile->getClientOriginalName();
            $safeFileName = transliterator_transliterate('any-latin; Latin-ASCII; [^A-Za-z0-9_] remove; lower()', $originalName);
            $newFileName = $safeFileName . '-' . uniqid() . '.' . $imageUploadedFile->guessExtension();
            try {
                // Déplace le fichier téléchargé vers le répertoire d'images spécifié
                $imageUploadedFile->move(
                    $this->getParameter('artwork_photo_path'),
                    $newFileName
                );
                dump($this->getParameter('artwork_photo_path') . '/' . $newFileName);
            } catch (FileException $e) {
                // Gère les erreurs liées au téléchargement du fichier
                dump($e->getMessage());
            }

            // Configure les informations de la photo
            $image->setPicture($originalName);
            $image->setPath($newFileName);

            // Configure les informations de l'œuvre d'art
            $artwork->setCreationDate($currentDate);
            $artwork->setMember($user);
            $form->getData()->setPhoto($image);

            // Persiste la photo et l'œuvre d'art en base de données
            $entityManager->persist($image);
            $entityManager->persist($artwork);
            $entityManager->flush();

            // Redirige vers la page 'Artwork' après l'ajout réussi
            return $this->redirectToRoute('artwork');
        }

        // Affiche la page 'add-element.html.twig' avec le formulaire d'ajout d'œuvre d'art
        return $this->render('pages/add-element.html.twig', [
            'artworkForm' => $form->createView(),
        ]);
    }


    #[Route('/favorite', name: 'favorite')]
    public function favorite(ArtworkRepository $artworkRepository, MemberRepository $memberRepository): Response
    {
        // Récupère l'utilisateur connecter
        $member = $this->getUser();
        // Récupère l'id du membre connecter
        $memberId = $memberRepository->find($member);

        // Trouve les artwork aimer par l'utilisateur connecter
        $likedArtworks = $artworkRepository->findLikedArtworksByUser($memberId);

        // Affiche la vue de la page des favoris avec les oeuvres aimer...
        return $this->render('pages/favorite.html.twig', [
            'likedArtworks' => $likedArtworks,
        ]);

    }

    #[Route('/artist-profile/{id}', name: 'artist_profile')]
    public function artistProfile(int $id, MemberRepository $memberRepository): Response
    {
        // Recherche un membre par son identifiant
        $member = $memberRepository->find($id);

        // Récupère les œuvres d'art du membre
        $artwork = $member->getArtworks();

        // Récupère la biographie du membre
        $biography = $member->getBiography();

        // Récupère les réseaux sociaux du membre
        $network = $member->getSocialNetworks();

        // Affiche la page 'artist_profile.html.twig' avec les données récupérées
        return $this->render('pages/artist_profile.html.twig', [
            'artworkEntities' => $artwork,
            'biographyEntity' => $biography,
            'networkEntities' => $network,
            'member' => $member,
        ]);
    }

    #[Route('/setting', name: 'setting')]
    public function parameter(): Response
    {
        // Récupère l'utilisateur connecté (membre)
        $member = $this->getUser();

        // Affiche la page 'setting.html.twig' en passant l'utilisateur connecté en tant que variable
        return $this->render('pages/setting.html.twig', [
            'member' => $member,
        ]);
    }

    #[Route('/report/{id}', name: 'report')]
    public function report(int $id, EntityManagerInterface $entityManager, Request $request, MemberRepository $memberRepository, ArtworkRepository $artworkRepository): Response
    {
        // Crée une nouvelle instance de l'entité Contact
        $report = new Report();

        // Récupère l'artwork par son id
        $artwork = $artworkRepository->find($id);

        // Récupère l'utilisateur connecté (membre)
        $member = $this->getUser();

        // Récupère l'identifiant du membre à partir du référentiel des membres
        $memberId = $memberRepository->find($member);

        // Crée un formulaire basé sur le ReportFormType, lié à l'entité Contact
        $form = $this->createForm(ReportFormType::class, $report);

        // Gère la soumission du formulaire
        $form->handleRequest($request);

        // Initialise la date actuelle
        $currentDate = new DateTime();

        // Vérifie si le formulaire a été soumis et est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Si un membre est connecté, associe le contact au membre
            if ($member) {
                $report->setMember($memberId);
            }
            $message = $form->get('report_message')->getData();
            $subject = $form->get('subject')->getData();

            // Définit les données du report
            $report->setDate($currentDate);
            $report->setReportMessage($message);
            $report->setReportSubject($subject);
            $report->setArtwork($artwork);

            // Persiste le report dans la base de données
            $entityManager->persist($report);
            $entityManager->flush();

            // Ajoute un message flash pour informer l'utilisateur du succès de l'envoi
            $this->addFlash('success', 'Votre message a été envoyé. Merci !');

            // Redirige vers la page de contact
            return $this->redirectToRoute('contact');
        }

        // Si le formulaire n'est pas soumis ou n'est pas valide, affiche la page de contact avec le formulaire
        return $this->render('pages/contact.html.twig', [
            'reportForm' => $form,
        ]);
    }

}