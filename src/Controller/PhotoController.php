<?php

namespace App\Controller;

use App\Repository\PhotoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PhotoController extends AbstractController
{
    #[Route('/doctrine/delete/photo/{id}', name: 'delete_photo')]
    public function DeleteMember(int $id, EntityManagerInterface $entityManager, PhotoRepository $photoRepository): Response
    {
        // Logique de suppression des photos

        // Recherche l'objet Photo dans le repository en fonction de l'ID
        $photo = $photoRepository->find($id);

        // Construit le chemin complet du fichier associé à la photo
        $filePath = $this->getParameter('artwork_photo_path') . '/' . $photo->getPath();

        // Vérifie si le fichier existe avant de le supprimer
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        // Répétition de la recherche de l'objet Photo (éventuellement redondant)
        $photo = $photoRepository->find($id);

        // Supprime l'objet Photo de la base de données
        $entityManager->remove($photo);
        $entityManager->flush();

        // Redirige vers la liste des photos
        return $this->redirectToRoute('photo');
    }
}
