<?php

namespace App\Repository;

use App\Entity\Artwork;
use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Artwork>
 *
 * @method Artwork|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artwork|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artwork[]    findAll()
 * @method Artwork[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtworkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artwork::class);
    }

    public function findLikedArtworksByUser(Member $member)
    {
        return $this->createQueryBuilder('artwork')
            ->join('artwork.likes', 'likes')
            ->where('likes = :user')
            ->setParameter('user', $member)
            ->getQuery()
            ->getResult();
    }

    public function searchArtworks(array $searchData)
    {
        $queryBuilder = $this->createQueryBuilder('a');

        // Si la recherche texte est fournie
        if (!empty($searchData['search'])) {
            $queryBuilder->andWhere('a.artwork_title LIKE :searchTerm')
                ->setParameter('searchTerm', '%' . $searchData['search'] . '%');
        }

        // Si des catégories sont fournies
        /** @var  ArrayCollection $searchData['categories'] */
        if (count($searchData['categories'])) {
            $queryBuilder->leftJoin('a.categories', 'c')
                ->andWhere('c.id IN (:categories)')
                ->setParameter('categories', $searchData['categories']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

//    /**
//     * @return Artwork[] Returns an array of Artwork objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Artwork
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
